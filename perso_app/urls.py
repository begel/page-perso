from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.accueil, name="accueil"),
    url(r'^publications$', views.publications, name="publications"),
    url(r'^contact$', views.contact, name="contact"),
    url(r'^envoi$', views.envoiMessage, name="envoi"),
]

