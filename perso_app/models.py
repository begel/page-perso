import datetime
from django.db import models
from django.utils import timezone

class Publication(models.Model):
    titre = models.CharField(max_length = 200)
    auteurs = models.CharField(max_length = 200)
    date = models.DateField()
    editeur = models.CharField(max_length = 200, blank = True)
    description = models.TextField()
    
    def __str__(self):
        return self.titre
    
    def recent(self):
        return (timezone.now() >= self.date >= timezone.now() - datetime.timedelta(days=240))

choices = (
    ('perso', 'Contact personel'),
    ('pro', 'Contact professionel')
)

class Contact(models.Model):
    nom = models.CharField(max_length = 50)
    mail = models.EmailField()
    raison = models.CharField(choices = choices, max_length = 50)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.nom
