from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Publication, Contact
from .forms import ContactForm

def accueil(request):
    return render(request, 'perso_app/index.html', {})

def publications(request):
    publications = Publication.objects.order_by('-date')
    context = {'publications': publications}
    return render(request, 'perso_app/publications.html', context)

def contact(request):
    context = {'form': ContactForm()}
    return render(request, 'perso_app/contact.html', context)

def envoiMessage(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            nom = form.cleaned_data['nom']
            mail = form.cleaned_data['mail']
            raison = form.cleaned_data['raison']
            message = form.cleaned_data['message']
            data = Contact(nom = nom, mail = mail, raison = raison, message = message)
            data.save()
            return HttpResponseRedirect(reverse('contact'))

