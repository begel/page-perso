from django.contrib import admin

from .models import Publication, Contact

admin.site.register(Publication)

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    readonly_fields = ['nom', 'mail', 'raison', 'message', 'date']
