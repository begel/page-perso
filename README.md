Création d'une page perso avec Django
=================

Ce tuto détaille l'utilisation de ce dépôt git comme base pour une page perso. Pour apprendre à créer un site Django de zéro ou pour agrémenter celui-ci, consultez la présentation du séminaire, disponible sur le [wiki](https://wiki.crans.org/CransTechnique/CransApprentis/SeminairesTechniques/2016-2017)

Ce dépôt git montre étape par étape la création de la page. Pour voir l'évolution, naviguez dans les commits.

Le tutoriel a été préparé pour un OS Linux, il peut y avoir des différences sur Windows.

Pré-requis
----------
### Serveur web
Il vous faut un serveur web installé en local sur votre ordinateur comme `apache`. Vérifiez que vous avez les permissions nécessaires sur le dossier `html`.

### Récupération du projet
Dans le dossier `html`, cloner le dépôt (ici utilisation en SSH) et rendez vous sur la bonne branche

    git clone git@gitlab.crans.org:begel/page-perso.git

###`virtualenv`
Ce site a été développé avec Python 3 et Django 1.10. Pour installer les bonnes versions pour ce projet spécifique, pas besoin d'une installation de Django 1.10 globalement, on peut le faire pour un dossier en particulier en utilisant `virtualenv`. Après installation du paquet, dans le dossier `html`

    cd page-perso
    virtualenv --python=python3 .env
    source .env/bin/activate
    pip install django

La commande source est à faire à chaque fois que vous travaillez sur le projet (et dans chaque terminal)

Utilisation du projet
---------------

Pour lancer le serveur, dans le dossier page-perso:

    ./manage.py runserver

Vous pouvez le laisser tourner dans un coin.

Pour créer la base de données:

    ./manage.py makemigrations
    ./manage.py migrate

A l'adresse `127.0.0.1:8000` dans un navigateur web, vous pouvez découvrir votre page perso.

Pour mettre en ligne votre site sur votre page perso Crans, tout est expliqué sur le [wiki](https://wiki.crans.org/PagesPerso/Django)

Personalisation du projet
-----------------
Pour enregistrer la liste des publications ou consultez les messages envoyés via le formulaire, vous pouvez vous rendre sur `127.0.0.1:8000/admin`. Pour vous connectez, vous aurez besoin d'un compte super utilisateur.

    ./manage.py createsuperuser

Pour personaliser le projet, vous pouvez aussi:

* Modifier les textes et les HTML en général en modifiant les gabarits dans `perso_app/templates/perso_app/`
* Modifier le style CSS: `perso_app/static/perso_app/style.vss`
* Ajouter/modifier des pages et modèles
